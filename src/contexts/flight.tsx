import React, { useReducer, createContext } from 'react';
import { FlightActionTypes, FlightState } from '../types/flight';
import { Flight } from '../models/flight';

interface Actions {
  type: string;
  payload?: any;
}

interface IContextProps {
  state: FlightState;
  dispatch: ({ type, payload }: {type: string, payload: any}) => void;
}

export const FlightContext = createContext({} as IContextProps);

const initialState: FlightState = {
  listFlights: new Flight({}),
  listCurrencies: [],
  listOriginPlaces: [],
  listDestinationPlaces: [],
  listCountries: [],
  loading: false,
  loadingOrigin: false,
  loadingDestination: false,
  error: null,
};

const reducer = (state: FlightState, action: Actions) => {
  switch (action.type) {
    case FlightActionTypes.SET_LOADING:
      return {
        ...state,
        loading: true,
      }
    case FlightActionTypes.SET_ORIGIN_LOADING:
      return {
        ...state,
        loadingOrigin: true,
      }
    case FlightActionTypes.SET_DESTINATION_LOADING:
      return {
        ...state,
        loadingDestination: true,
      }
    case FlightActionTypes.SET_ALL:
      return {
        ...state,
        listFlights: new Flight(action.payload),
        loading: false,
      };
    case FlightActionTypes.SET_CURRENCIES:
      return {
        ...state,
        listCurrencies: action.payload,
      };
    case FlightActionTypes.SET_ORIGIN_PLACES:
      return {
        ...state,
        listOriginPlaces: action.payload,
        loadingOrigin: false,
      };
    case FlightActionTypes.SET_DESTINATION_PLACES:
      return {
        ...state,
        listDestinationPlaces: action.payload,
        loadingDestination: false,
      };
    case FlightActionTypes.SET_COUNTRIES:
        return {
          ...state,
          listCountries: action.payload,
        };
    case FlightActionTypes.ERROR_REQUEST:
      return {
        ...state,
        loading: false,
        loadingOrigin: false,
        loadingDestination: false,
        error: action.payload,
      };
    default:
      throw new Error();
  }
};

export const FlightContextProvider = (props: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <FlightContext.Provider value={{ state, dispatch }}>
      {props.children}
    </FlightContext.Provider>
  );
};
