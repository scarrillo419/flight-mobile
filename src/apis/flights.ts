import axios from 'axios';

// @ts-ignore
import { API_URL } from 'react-native-dotenv';
import { setHeaders } from '../utils';

const apiurl = API_URL ?? '';

export default {
  async getFlights(payload: any) {
    setHeaders(); // Debe colocarse en otro lado del proyecto
    const { data } = await axios.get(
      `${apiurl}/browsequotes/v1.0/${payload.country}/${payload.currency}/${payload.locale}/${payload.originplace}/${payload.destinationplace}/${payload.outboundpartialdate}/`,
      { params: { query: payload.query } },
    );
    return data;
  },
  async getPlaces(payload: any) {
    setHeaders();
    const { data } = await axios.get(`${apiurl}/autosuggest/v1.0/${payload.country}/${payload.currency}/${payload.locale}/`, { params: { query: payload.query } });
    return data;
  },
  async getCurrencies(payload: any) {
    setHeaders();
    const { data } = await axios.get(`${apiurl}/reference/v1.0/currencies`);
    return data;
  },
  async getCountries(payload: any) {
    setHeaders();
    const { data } = await axios.get(`${apiurl}/reference/v1.0/countries/en-US/`);
    return data;
  }
};
