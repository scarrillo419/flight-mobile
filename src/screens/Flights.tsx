import React, { memo, useContext, useEffect, useCallback, useState } from 'react';
import moment from 'moment';
import { isEmpty } from 'lodash';
import { useForm } from 'react-hook-form';
import I18n from 'react-native-i18n';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { Header } from 'react-native/Libraries/NewAppScreen';
import { Picker } from '@react-native-community/picker';
import DateTimePicker from '@react-native-community/datetimepicker';

import { Col, Row, Grid } from 'react-native-easy-grid';

import { FlightContext } from '../contexts/flight';
import { getCountries, getCurrencies, getFlights, getPlaces } from '../services/flight';
import { prepareData } from '../utils';

import { styles } from '../assets/styles/flight';
import { errorRequest } from '../actions/flight';

interface Props {
  navigation: any;
}

const Flights: React.FC<Props> = memo(({ navigation }) => {
  const { state, dispatch } = useContext(FlightContext);
  const { register, handleSubmit, setValue, errors } = useForm({
    defaultValues: {
      currency: undefined,
      origincountry: undefined,
      originplace: undefined,
      destinationcountry: undefined,
      destinationplace: undefined,
      outboundpartialdate: new Date(),
      inboundpartialdate: new Date(),
    },
  });
  const [currency, setCurrency] = useState(undefined);
  const [oCountry, setOCountry] = useState(undefined);
  const [oPlace, setOPlace] = useState(undefined);
  const [dCountry, setDCountry] = useState(undefined);
  const [dPlace, setDPlace] = useState(undefined);
  const [showODate, setShowODate] = useState(false);
  const [showIDate, setShowIDate] = useState(false);
  const [outDate, setOutDate] = useState(new Date());
  const [inDate, setInDate] = useState(new Date());
  const {
    loading,
    loadingOrigin,
    loadingDestination,
    listOriginPlaces,
    listDestinationPlaces,
    listCurrencies,
    listCountries,
  } = state;

  useEffect(() => {
    register({ name: 'currency' }, { required: true });
    register({ name: 'origincountry' }, { required: true });
    register({ name: 'originplace' }, { required: true });
    register({ name: 'destinationcountry' }, { required: true });
    register({ name: 'destinationplace' }, { required: true });
    register({ name: 'outboundpartialdate' }, { required: true });
    register({ name: 'inboundpartialdate' });

    getCountries(dispatch);
    getCurrencies(dispatch);
  }, [dispatch, register]);

  const onChangeOCountry = useCallback(name => (value: any) => {
    setValue(name, value);
    setOCountry(value);

    if (currency) {
      const payload = {
        currency,
        country: value,
        query: value,
        locale: 'en-US',
      };
      fetchPlaces(payload, 'origin');
    }
  }, [currency]);

  const onChangeOPlace = useCallback(name => (value: any) => {
    setValue(name, value);
    setOPlace(value);
  }, []);

  const onChangeDCountry = useCallback(name => (value: any) => {
    const { locale } = I18n;
    setValue(name, value);
    setDCountry(value);

    if (currency) {
      const payload = {
        currency,
        locale,
        country: value,
        query: value,
      };
      fetchPlaces(payload, 'destination');
    }
  }, [currency]);

  const onChangeDPlace = useCallback(name => (value: any) => {
    setValue(name, value);
    setDPlace(value);
  }, []);

  const onChangeODate = useCallback(name => (event: any, selectedDate: any) => {
    setShowODate(false);
    setOutDate(selectedDate);
    setValue(name, selectedDate);
  }, []);

  const onChangeIDate = useCallback(name => (event: any, selectedDate: any) => {
    setShowIDate(false);
    setInDate(selectedDate);
    setValue(name, selectedDate);
  }, []);

  const onChangeCurrency = (name: any) => (value: any) => {
    setValue(name, value);
    setCurrency(value);
  };
  
  const fetchPlaces = useCallback((payload, target) => {
    getPlaces(dispatch, { payload, target });
  }, [dispatch]);
  
  const fetchFlights = useCallback((fields: any) => {
    const payload = prepareData(fields);
    getFlights(dispatch, payload)
      .then(() => navigation.navigate('Results'))
      .catch((e) => dispatch(errorRequest(e)));
  }, [dispatch]);
  
  return (
    <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      style={styles.scrollView}
    >
      <View style={styles.body}>
        <View style={styles.sectionContainer}>
          <Grid>
            <Row>
              {/* CURRENCY */}
              <Col style={styles.wrapperCol}>
                <Text style={styles.textTitle}>Moneda</Text>
                <View style={styles.wrapperView}>
                  <Picker
                    selectedValue={currency}
                    style={styles.picker}
                    onValueChange={onChangeCurrency('currency')}
                    enabled={!isEmpty(listCurrencies)}
                  >
                    <Picker.Item label="Seleccione" value={''} />
                    {listCurrencies?.map(c => (<Picker.Item key={c.Code} label={`${c.Code} (${c.Symbol})`} value={c.Code} />))}
                  </Picker>
                </View>
                {(errors.currency && !currency) && <Text style={styles.errorText}>Campo requerido</Text>}
              </Col>
            </Row>
            <Row>
              <Col style={styles.wrapperCol}>
                <Text style={styles.textTitle}>Origen</Text>
                <View>
                  <Text style={styles.textLabel}>Pais</Text>
                  <View style={styles.wrapperView}>
                    <Picker
                      selectedValue={oCountry}
                      style={styles.picker}
                      onValueChange={onChangeOCountry('origincountry')}
                      enabled={!isEmpty(listCountries) && !isEmpty(currency)}
                    >
                      <Picker.Item label="Seleccione" value={''} />
                      {listCountries?.map(c => (<Picker.Item key={c.Code} label={`${c.Name} (${c.Code})`} value={c.Code} />))}
                    </Picker>
                  </View>
                  {(errors.origincountry && !oCountry) && <Text style={styles.errorText}>Campo requerido</Text>}
                  <Text style={styles.textLabel}>Lugar</Text>
                  <View style={styles.wrapperView}>
                    <Picker
                      selectedValue={oPlace}
                      style={styles.picker}
                      onValueChange={onChangeOPlace('originplace')}
                      enabled={!isEmpty(listOriginPlaces) && !loadingOrigin}
                    >
                      <Picker.Item label="Seleccione" value={''} />
                      {listOriginPlaces?.map(c => (<Picker.Item key={c.PlaceId} label={`${c.PlaceName} (${c.PlaceId})`} value={c.PlaceId} />))}
                    </Picker>
                  </View>
                  {(errors.originplace && !oPlace) && <Text style={styles.errorText}>Campo requerido</Text>}
                </View>
              </Col>
            </Row>
            <Row>
              <Col style={styles.wrapperCol}>
                <Text style={styles.textTitle}>Destino</Text>
                <View>
                  <Text style={styles.textLabel}>Pais</Text>
                  <View style={styles.wrapperView}>
                    <Picker
                      selectedValue={dCountry}
                      style={styles.picker}
                      onValueChange={onChangeDCountry('destinationcountry')}
                      enabled={!isEmpty(listCountries) && !isEmpty(currency)}
                    >
                      <Picker.Item label="Seleccione" value={''} />
                      {listCountries?.map(c => (<Picker.Item key={c.Code} label={`${c.Name} (${c.Code})`} value={c.Code} />))}
                    </Picker>
                  </View>
                  {(errors.destinationcountry && !dCountry) && <Text style={styles.errorText}>Campo requerido</Text>}
                  <Text style={styles.textLabel}>Lugar</Text>
                  <View style={styles.wrapperView}>
                    <Picker
                      selectedValue={dPlace}
                      style={styles.picker}
                      onValueChange={onChangeDPlace('destinationplace')}
                      enabled={!isEmpty(listDestinationPlaces) && !loadingDestination}
                    >
                      <Picker.Item label="Seleccione" value={''} />
                      {listDestinationPlaces?.map(c => (<Picker.Item key={c.PlaceId} label={`${c.PlaceName} (${c.PlaceId})`} value={c.PlaceId} />))}
                    </Picker>
                  </View>
                  {(errors.destinationplace && !dPlace) && <Text style={styles.errorText}>Campo requerido</Text>}
                </View>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={styles.textTitle}>Fechas</Text>
                <Row>
                  <Col style={styles.wrapperCol}>
                    <Text style={styles.textLabel}>Fecha de partida</Text>
                    <View style={styles.wrapperView}>
                      <TouchableOpacity
                        style={styles.buttonPicker}
                        onPress={() => setShowODate(true)}
                      >
                        <Text style={styles.searchButtonText}>{`${moment(outDate).format('DD MMMM YYYY') ?? 'Fecha de partida'}`}</Text>
                      </TouchableOpacity>
                      {showODate && <DateTimePicker
                        value={outDate}
                        mode="date"
                        display="default"
                        onChange={onChangeODate('outboundpartialdate')}
                      />}
                    </View>
                    {(errors.outboundpartialdate && !outDate) && <Text style={styles.errorText}>Campo requerido</Text>}
                  </Col>
                  <Col style={styles.wrapperCol}>
                    <Text style={styles.textLabel}>Fecha de regreso</Text>
                    <View style={styles.wrapperView}>
                      <TouchableOpacity
                        style={styles.buttonPicker}
                        onPress={() => setShowIDate(true)}
                      >
                        <Text style={styles.searchButtonText}>{`${moment(inDate).format('DD MMMM YYYY') ?? 'Fecha de regreso'}`}</Text>
                      </TouchableOpacity>
                      {showIDate && <DateTimePicker
                        value={inDate}
                        mode="date"
                        display="default"
                        onChange={onChangeIDate('inboundpartialdate')}
                      />}
                    </View>
                    {(errors.inboundpartialdate && !inDate) && <Text style={styles.errorText}>Campo requerido</Text>}
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col style={styles.wrapperCol}>
                <TouchableOpacity
                  onPress={handleSubmit(fetchFlights)}
                  disabled={loadingOrigin || loadingDestination || loading}
                  style={styles.searchButtonWrapper}
                >
                  {loading
                    ? <ActivityIndicator size="large" color="#fff" />
                    : <Text style={styles.searchButtonText}>Buscar</Text>}
                </TouchableOpacity>
              </Col>
            </Row>
          </Grid>
        </View>
      </View>
    </ScrollView>
  );
});

export default Flights;
