import React, { memo, useContext } from 'react';
import moment from 'moment';
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
} from 'react-native';

import { FlightContext } from '../contexts/flight';
import { styles } from '../assets/styles/result';

const Results: React.FC = memo(() => {
  const { state } = useContext(FlightContext);
  const { listFlights } = state;

  const getPlace = (placeId: number) => {
    const { Places } = listFlights;
    const place: any = Places.find(p => p.PlaceId === placeId);
    return place.Name ?? '';
  }

  const getCarrier = (carrierIds: number[]) => {
    const { Carriers } = listFlights;
    return Carriers
      .filter(c => carrierIds.some(ci => ci === c.CarrierId))
      .map(c => c.Name)
      .join(', ');
  };

  const renderItem = (item: any) => (
    <View style={styles.itemWrapper}>
      <Text style={styles.textPrice}>{`Precio: ${listFlights.Currencies[0].Symbol} ${item.item.MinPrice}`}</Text>
      <Text style={styles.textItem}>{`Fecha de partida: ${moment(item.item.OutboundLeg.DepartureDate).format('DD MMMM YYYY')}`}</Text>
      <Text style={styles.textItem}>{`Origen: ${getPlace(item.item.OutboundLeg.OriginId)}`}</Text>
      <Text style={styles.textItem}>{`Destino ${getPlace(item.item.OutboundLeg.DestinationId)}`}</Text>
      <Text style={styles.textItem}>{`Aerolínea: ${getCarrier(item.item.OutboundLeg.CarrierIds)}`}</Text>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.list}
        keyExtractor={item => item.QuoteId.toString()}
        data={listFlights.Quotes ?? []}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
});

export default Results;
