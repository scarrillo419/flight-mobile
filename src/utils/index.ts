import axios, { AxiosError } from 'axios';
import moment from 'moment';
import { isEmpty } from 'lodash';
import I18n from 'react-native-i18n';

// @ts-ignore
import { RAPIDAPI_HOST, RAPIDAPI_KEY } from 'react-native-dotenv';

export const setHeaders = () => {
  axios.defaults.headers.common['x-rapidapi-host'] = RAPIDAPI_HOST;
  axios.defaults.headers.common['x-rapidapi-key']  = RAPIDAPI_KEY;
};

export const prepareData = (fields: any) => {
  const { locale } = I18n;

  fields.country = fields.origincountry;
  fields.outboundpartialdate = moment(fields.outboundpartialdate).format('YYYY-MM-DD');
  if (fields.inboundpartialdate) {
    fields.query = moment(fields.inboundpartialdate).format('YYYY-MM-DD');
    delete fields.inboundpartialdate;
  }
  fields.locale = locale;

  delete fields.origincountry;
  delete fields.destinationcountry;
  return fields;
};

export const manageError = (error: AxiosError) => {
  if (error.response) {
    const { data, statusText, status } = error.response;

    if (data) {
      if ('ValidationErrors' in data) {
        return data.ValidationErrors.map((e: any) => `${e.ParameterName} (${e.ParameterValue}): ${e.Message}`).join('. ');
      }
      if ('message' in data) {
        return data.message;
      }
    }

    if (!isEmpty(statusText)) return `${statusText} (${status})`;

    return JSON.stringify(error.response);
  }

  return 'Error desconocido. Intente de nuevo o contacte al administrador.';
};
