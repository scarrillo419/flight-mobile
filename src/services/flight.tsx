import api from '../apis/flights';
import Toast from 'react-native-simple-toast';
import { isEmpty } from 'lodash';
import {
  setLoading,
  setAll,
  setCurrencies,
  setCountries,
  setOriginPlaces,
  setDestinationPlaces,
  setDestinationLoading,
  setOriginLoading,
  errorRequest,
} from '../actions/flight';
import { manageError } from '../utils';

export const getFlights = (dispatch: any, payload: any) => {
  return new Promise(async (resolve, reject) => {
    dispatch(setLoading());
    try {
      const data = await api.getFlights(payload);

      if (isEmpty(data.Quotes)) {
        Toast.show('No se encontraron resultados con los datos introducidos');
        reject();
        return;
      }

      dispatch(setAll(data));
      resolve();
    } catch (error) {
      dispatch(errorRequest(error));
      Toast.show(manageError(error));
      reject();
    }
  });
};

export const getPlaces = async (dispatch: any, payload: any) => {
  if (payload.target === 'origin') dispatch(setOriginLoading());
  else dispatch(setDestinationLoading());
  try {
    const data = await api.getPlaces(payload.payload);

    if (payload.target === 'origin') dispatch(setOriginPlaces(data.Places ?? []));
    else dispatch(setDestinationPlaces(data.Places ?? []));
  } catch (error) {
    dispatch(errorRequest(error));
    Toast.show(manageError(error));
  }
};

export const getCurrencies = async (dispatch: any, payload?: any) => {
  try {
    const data = await api.getCurrencies(payload);
    dispatch(setCurrencies(data.Currencies ?? []));
  } catch (error) {
    dispatch(errorRequest(error));
    Toast.show(manageError(error));
  }
};

export const getCountries = async (dispatch: any, payload?: any) => {
  try {
    const data = await api.getCountries(payload);
    dispatch(setCountries(data.Countries ?? []));
  } catch (error) {
    dispatch(errorRequest(error));
    Toast.show(manageError(error));
  }
};
