export class Place {
  public CityId: string;
  public CountryId: string;
  public CountryName: string;
  public PlaceId: string;
  public PlaceName: string;
  public RegionId: string;

  constructor(data: any) {
    this.CityId = data.CityId;
    this.CountryId = data.CountryId;
    this.CountryName = data.CountryName;
    this.PlaceId = data.PlaceId;
    this.PlaceName = data.PlaceName;
    this.RegionId = data.RegionId;
  }
}
