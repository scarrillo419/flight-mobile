import { Currency } from './currency';

class Carrier {
  public CarrierId: number;
  public Name: string;

  constructor(data: any) {
    this.CarrierId = data.CarrierId;
    this.Name = data.Name;
  }
}

class Place {
  public CityId: string;
  public CityName: string;
  public CountryName: string;
  public IataCode: string;
  public Name: string;
  public PlaceId: number;
  public SkyscannerCode: string;
  public Type: string;

  constructor(data: any) {
    this.CityId = data.CityId;
    this.CityName = data.CityName;
    this.CountryName = data.CountryName;
    this.IataCode = data.IataCode;
    this.Name = data.Name;
    this.PlaceId = data.PlaceId;
    this.SkyscannerCode = data.SkyscannerCode;
    this.Type = data.Type;
  }
}

class Quote {
  public Direct: boolean;
  public MinPrice: number;
  public OutboundLeg: any;
  public QuoteDateTime: string;
  public QuoteId: number;

  constructor(data: any) {
    this.Direct = data.Direct;
    this.MinPrice = data.MinPrice;
    this.OutboundLeg = data.OutboundLeg;
    this.QuoteDateTime = data.QuoteDateTime;
    this.QuoteId = data.QuoteId;
  }
}

export class Flight {
  public Carriers: Carrier[];
  public Currencies: Currency[];
  public Places: Place[];
  public Quotes: Quote[];

  constructor(data: any) {
    this.Carriers = data.Carriers;
    this.Currencies = data.Currencies;
    this.Places = data.Places;
    this.Quotes = data.Quotes;
  }
}