export class Country {
  public Code: string;
  public Name: string;

  constructor(data: any) {
    this.Code = data.Code;
    this.Name = data.Name;
  }
}
