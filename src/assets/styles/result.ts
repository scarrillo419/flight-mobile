import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  body: {
    backgroundColor: 'white',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  itemWrapper: {
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
    paddingBottom: 12,
  },
  textPrice: {
    fontSize: 18,
    fontWeight: '700',
  },
  textItem: {
    fontSize: 16,
  },
  list: {
    backgroundColor: 'white',
    paddingTop: 10,
    marginBottom: 20,
    paddingHorizontal: 20,
  },
});
