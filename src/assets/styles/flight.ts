import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
  body: {
    backgroundColor: 'white',
  },
  sectionContainer: {
    marginTop: 12,
    paddingHorizontal: 24,
  },
  picker: {
    height: 50,
    width: '100%',
  },
  wrapperView: {
    borderWidth: 1,
    borderColor: '#e3e3e3',
    borderRadius: 4,
  },
  wrapperCol: {
    padding: 5,
  },
  errorText: {
    color: 'red',
    marginBottom: 5,
  },
  searchButtonWrapper: {
    height: 45,
    backgroundColor: '#007acc',
    borderRadius: 4,
    marginTop: 25,
  },
  searchButtonText: {
    textAlign: 'center',
    lineHeight: 45,
    color: 'white',
    fontSize: 18,
  },
  margingBottom: {
    marginBottom: 10,
  },
  buttonPicker: {
    height: 40,
    backgroundColor: '#007acc',
    borderRadius: 4,
  },
  textLabel: {
    fontSize: 18,
    color: '#2a2a2a',
  },
  textTitle: {
    fontSize: 26,
    fontWeight: '700',
    color: '#2a2a2a',
    borderBottomColor: '#e3e3e3',
    borderBottomWidth: 1,
    marginTop: 10,
    marginBottom: 10,
  },
});
