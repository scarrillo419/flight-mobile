import React, { memo } from 'react';
import { useWindowDimensions, View, Text } from 'react-native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import { Icon } from 'react-native-elements';

import Flights from '../screens/Flights';
import Results from '../screens/Results';

import { FlightContextProvider } from '../contexts/flight';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const DrawerNavigator: React.FC = memo(() => {
  const dimensions = useWindowDimensions();

  const DrawerContent = (props: any) => (
    <DrawerContentScrollView {...props}>
      {/* Logo / Estilos */}
      <View style={{ width: '100%', height: 150, backgroundColor: '#e3e3e3' }} />
      <DrawerItemList {...props} />
    </DrawerContentScrollView>
  );

  const FlightScreen = ({ navigation }: any) => (
    <Stack.Navigator>
      <Stack.Screen
        name="Search"
        component={Flights}
        options={{
          headerTitle: 'Buscar Vuelos',
          headerLeftContainerStyle: {
            marginLeft: 15,
          },
          headerLeft: () => (
            <Icon
              onPress={() => navigation.toggleDrawer()}
              type="font-awesome"
              name="bars"
            />
          )
        }}
      />
      <Stack.Screen
        name="Results"
        component={Results}
        options={{
          headerTitle: 'Resultados',
          headerLeftContainerStyle: {
            marginLeft: 15,
          },
          headerLeft: () => (
            <Icon
              onPress={() => navigation.toggleDrawer()}
              type="font-awesome"
              name="bars"
            />
          )
        }}
      />
    </Stack.Navigator>
  );

  return (
    <FlightContextProvider>
      <NavigationContainer>
        <Drawer.Navigator
          drawerType={dimensions.width >= 768 ? 'permanent' : 'front'}
          drawerContent={props => DrawerContent(props)}
        >
          <Drawer.Screen
            name="Flights"
            component={FlightScreen}
            options={{
              title: 'Vuelos',
            }}
          />
        </Drawer.Navigator>
      </NavigationContainer>
    </FlightContextProvider>
  );
});

export default DrawerNavigator;
