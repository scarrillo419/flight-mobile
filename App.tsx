/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect, memo } from 'react';

import SplashScreen from 'react-native-splash-screen';

import DrawerNavigation from './src/navigation/DrawerNavigation';
// import { setHeaders } from './src/utils';

const App = memo(() => {
  useEffect(() => {
    // setHeaders();
    SplashScreen.hide();
  }, []);

  return (
    <DrawerNavigation />
  );
});

export default App;
